terraform {
  backend "s3" {
    bucket = "ejemplojaime"
    key    = "recipe-app-tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}
